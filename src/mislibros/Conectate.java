/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mislibros;
import java.sql.*;
/**
 *
 * @author kafalca
 */
public class Conectate {
    static String bd = "mis_libros";
    static String login = "root";
    static String password = "root";
    static String url = "jdbc:mysql://localhost/"+bd;
    
    Connection conn = null;
    
    public Conectate()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url,login,password);
            if (conn!=null)
                System.out.println("Conexión a base de datos " + bd + " lista.");
        }catch(SQLException e)
        {
            System.out.println(e);
        }catch(ClassNotFoundException e)
        {
            System.out.println(e);
        }
    }
    
    public Connection getConnection()
    {
        return conn;
    }
    
    public void desconectar()
    {
        conn = null;
    }
}
